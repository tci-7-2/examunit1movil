package com.example.examenunit1c1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CuentaBancoActivity extends AppCompatActivity {
    private EditText txtNumCuenta, txtNombre, txtBanco, txtSaldo, txtCantidad, lblNewSaldo;
    private Spinner spinnerMovimientos;
    private Button btnAplicar, btnRegresar, btnLimpiar;
    private TextView lblNombre;

    private CuentaBanco cuentaBanco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);

        lblNombre = findViewById(R.id.lblNombre);

        String username = getIntent().getStringExtra("USERNAME");
        lblNombre.setText("Usuario: " + username);

        txtNumCuenta = findViewById(R.id.txtNumCuenta);
        txtNombre = findViewById(R.id.txtNombre);
        txtBanco = findViewById(R.id.txtBanco);
        txtSaldo = findViewById(R.id.txtSaldo);
        spinnerMovimientos = findViewById(R.id.spinnerMovimientos);
        txtCantidad = findViewById(R.id.txtCantidad);
        lblNewSaldo = findViewById(R.id.lblNewSaldo);
        btnAplicar = findViewById(R.id.btnAplicar);
        btnRegresar = findViewById(R.id.btnRegresar);
        btnLimpiar = findViewById(R.id.btnLimpiar);

        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aplicarTransaccion();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }

    private void aplicarTransaccion() {
        String numCuenta = txtNumCuenta.getText().toString();
        String nombre = txtNombre.getText().toString();
        String banco = txtBanco.getText().toString();
        String saldoStr = txtSaldo.getText().toString();

        if (numCuenta.isEmpty() || nombre.isEmpty() || banco.isEmpty() || saldoStr.isEmpty()) {
            Toast.makeText(this, "Por favor, complete todos los campos de la cuenta", Toast.LENGTH_SHORT).show();
            return;
        }

        float saldo;
        try {
            saldo = Float.parseFloat(saldoStr);
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Ingrese un saldo válido", Toast.LENGTH_SHORT).show();
            return;
        }

        cuentaBanco = new CuentaBanco(numCuenta, nombre, banco, saldo);

        String selectedItem = spinnerMovimientos.getSelectedItem().toString();
        String amountStr = txtCantidad.getText().toString();
        if (amountStr.isEmpty()) {
            Toast.makeText(this, "Ingrese una cantidad", Toast.LENGTH_SHORT).show();
            return;
        }

        float amount;
        try {
            amount = Float.parseFloat(amountStr);
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Ingrese una cantidad válida", Toast.LENGTH_SHORT).show();
            return;
        }

        switch (selectedItem) {
            case "Consultar":
                lblNewSaldo.setText(String.valueOf(cuentaBanco.obtenerSaldo()));
                break;
            case "Depositar":
                cuentaBanco.hacerDeposito(amount);
                lblNewSaldo.setText(String.valueOf(cuentaBanco.obtenerSaldo()));
                break;
            case "Retirar":
                try {
                    cuentaBanco.retirarDinero(amount);
                    lblNewSaldo.setText(String.valueOf(cuentaBanco.obtenerSaldo()));
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                Toast.makeText(this, "Seleccione una opción válida", Toast.LENGTH_SHORT).show();
                break;
        }

        txtSaldo.setText(String.valueOf(cuentaBanco.getSaldo()));
    }

    private void limpiar() {
        txtNumCuenta.setText("");
        txtNombre.setText("");
        txtBanco.setText("");
        txtSaldo.setText("");
        txtCantidad.setText("");
        lblNewSaldo.setText("");
    }
}
